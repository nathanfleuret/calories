import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:calories/widgets/custom_text.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isMan = false;
  Color mainColor = Colors.redAccent;
  DateTime birthday;
  int age;
  double height = 100.0;
  int weight;
  int chosenItem;
  double caloriesWithActivity;
  double calories;
  Map mapActivite = {
    0: "Faible",
    1: "Modéré",
    2: "Forte",
  };

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      print("Nous sommes sur IOS");
    } else {
      print("Nous ne sommes pas sur IOS");
    }

    return new GestureDetector(
      onTap: (() => FocusScope.of(context).requestFocus(new FocusNode())),
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: mainColor,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              padding(),
              CustomText(
                  "Remplissez tous les champs pour obtenir votre besoin journalier en calories."),
              padding(),
              Card(
                elevation: 9.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    padding(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        //padding: const EdgeInsets.all(25),
                        CustomText(
                          "Femme",
                          color: Colors.redAccent,
                        ),
                        Switch(
                          inactiveTrackColor: Colors.redAccent,
                          value: isMan,
                          onChanged: (bool b) {
                            setState(() {
                              isMan = b;
                              mainColor =
                                  (isMan) ? Colors.blue : Colors.redAccent;
                            });
                          },
                        ),
                        CustomText(
                          "Homme",
                          color: Colors.blue,
                        ),
                      ],
                    ),
                    padding(),
                    RaisedButton(
                      onPressed: showDate,
                      color: mainColor,
                      child: Text(
                        (birthday == null)
                            ? "Appuyez pour entrer votre age"
                            : "Votre age est de : $age",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    padding(),
                    CustomText(
                      "Votre taille est de : $height cm.",
                      color: mainColor,
                    ),
                    padding(),
                    Slider(
                        value: height,
                        activeColor: mainColor,
                        min: 100.0,
                        max: 220.0,
                        divisions: 120,
                        onChanged: (double d) {
                          setState(() {
                            height = d;
                          });
                        }),
                    padding(),
                    TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (String s) {
                        setState(() {
                          weight = int.tryParse(s);
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "Entrez votre poids en kilos.",
                      ),
                    ),
                    padding(),
                    CustomText(
                      "Quelle est votre activité sportive?",
                      color: mainColor,
                    ),
                    radioRow(),
                  ],
                ),
              ),
              padding(),
              RaisedButton(
                onPressed: calculateCalories,
                child: CustomText("Calculer", color: Colors.white),
                color: mainColor,
              )
            ],
          ),
        ),
      ),
    );
  }

  Padding padding() {
    return new Padding(padding: EdgeInsets.only(top: 20.0));
  }

  Row radioRow() {
    List<Widget> l = [];
    mapActivite.forEach((key, value) {
      Column column = new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Radio(
              activeColor: mainColor,
              value: key,
              groupValue: chosenItem,
              onChanged: (Object i) {
                setState(() {
                  chosenItem = i;
                });
              }),
          CustomText(value, color: mainColor)
        ],
      );
      l.add(column);
    });
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: l,
    );
  }

  Future<Null> showDate() async {
    DateTime choice = await showDatePicker(
        context: context,
        initialDatePickerMode: DatePickerMode.year,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(1930),
        lastDate: new DateTime.now());
    if (choice != null) {
      setState(() {
        birthday = choice;
        age = calculateAge(birthday);
      });
    }
  }

  int calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }

  Future<Null> calculateCalories() async {
    if (age != null && height != null && weight != null && chosenItem != null) {
      if (isMan) {
        calories =
            66.4730 + (13.7516 * weight) + (5.0033 * height) - (6.7550 * age);
      } else {
        calories =
            655.0955 + (9.5634 * weight) + (1.8496 * height) - (4.6756 * age);
      }
      switch (chosenItem) {
        case 0:
          caloriesWithActivity = calories * 1.2;
          break;
        case 1:
          caloriesWithActivity = calories * 1.5;
          break;
        case 2:
          caloriesWithActivity = calories * 1.8;
          break;
      }
      setState(() {
        dialogue();
      });
    } else {
      alerte();
    }
  }

  Future<Null> dialogue() async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new SimpleDialog(
            title: CustomText(
              "Votre besoin en calories",
              color: mainColor,
            ),
            contentPadding: EdgeInsets.all(15.0),
            children: <Widget>[
              padding(),
              CustomText("Votre besoin est de $calories calories."),
              padding(),
              CustomText(
                  "Votre besoin avec activité sportive est de $caloriesWithActivity calories."),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: CustomText(
                    "OK",
                    color: Colors.red,
                  ))
            ],
          );
        });
  }

  Future<Null> alerte() async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: CustomText(
              "Erreur",
              factor: 1.1,
            ),
            content: CustomText("Tous les champs ne sont pas remplis."),
            contentPadding: EdgeInsets.all(15.0),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: CustomText(
                    "OK",
                    color: Colors.red,
                  ))
            ],
          );
        });
  }
}
